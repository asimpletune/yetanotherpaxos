import java.io.IOException;
import java.net.SocketTimeoutException;

	class ClientRouter extends Listener
	{				
		private Server parent;
		public ClientRouter(Server parent, Channel channel) throws IOException 
		{
			super(channel);
			this.parent = parent;
			this.socket.setSoTimeout(HeartBeat.DEFAULT_HEARTBEAT_INTERVAL << 1);		
		}

		@Override
		public void switchBoard(String msg) throws IOException 
		{		
			String[] tokenized = msg.split(" ");				
			if (tokenized.length > 0)
			{
				String command = tokenized[0];
				switch(command)
				{
					case Cmd.TIMEOUT:
					{
						broadcast( Cmd.PREPARE + " " + (parent.state.prepareNum() + 1) );
						break;
					}
					case Cmd.CLIENT_OUT:
					{
						System.out.println("CLIENT_OUT");
						parent.msgBuffer.offer(msg);
						if (parent.state.getState() == States.ACCEPTING_CLIENT)
						{
							System.out.println("CLIENTOUT: " + msg);
							int slotNumber = parent.state.history().size();
							String value = msg.substring(Cmd.CLIENT_OUT.length() + 1);
							String acceptMsg = String.format("%s %d %d %s %s", Cmd.ACCEPT, parent.state.prepareNum(), slotNumber, "false", value);
							broadcast(acceptMsg);
						}
						break;
					}
					default: // ignore all
					{
						break;
					}
				}
			}
		}		

		@Override
		public String getIncomingMsg() throws IOException
		{
			try
			{
				return super.getIncomingMsg();
			}
			catch (SocketTimeoutException s)
			{
				return Cmd.TIMEOUT;
			}			
		}
	}