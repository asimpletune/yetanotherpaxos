import java.util.Hashtable;

public class ServerState {
	
	public static int NUM_SERVERS;
	public static int QUORUM;
	private int serverID;
	private int prepareNumber;
	private History history;	
	private States currentState;
	protected Hashtable<Integer, History> collectedHistories;
	protected CleanUp cleanUpThread;
	protected boolean cleaningUp;
	protected int ackCount;
	
	public ServerState(int serverID)
	{		
		currentState = States.INITIAL;
		NUM_SERVERS = Main.NUM_SERVERS;
		QUORUM = NUM_SERVERS/2 + 1;
		this.serverID = serverID;
		prepareNumber = 0;	
		history = new History();
		history.add(new Proposal(0, "NOOP"));
		cleaningUp = false;
		collectedHistories = new Hashtable<Integer, History>();
	}
	
	public States getState()
	{
		return currentState;
	}
	
	public boolean quorum() throws IllegalStateException
	{
		switch (currentState)
		{
			case COLLECTING_HISTORY:
			{
				return collectedHistories.size() >= QUORUM;			
			}
			default:
			{
				throw new IllegalStateException("You're not in a state");
			}
		}
	}
		
	public void setState(States state)
	{
		System.out.println("SETTING STATE TO: " + state);
		currentState = state;
	}
	
	public History history()
	{
		return history;
	}
		
	public void prepareNum(int n)
	{
		assert n > -1;
		prepareNumber = n;
	}
	
	public int prepareNum()
	{
		return prepareNumber;
	}
	
	public int serverID()
	{
		return serverID;
	}
	
	protected boolean isLeader()
	{
		System.out.println("serverID: " + serverID);
//		System.out.println("RESULT: " + (serverID == NUM_SERVERS ));
		return serverID == prepareNumber % NUM_SERVERS;
	}	
}



