
public class Cmd 
{
	protected static final String CLIENT_OUT = "CLIENT>";
	protected static final String CLIENT_IN = ">CLIENT";
	protected static final String HEARTBEAT_MSG = "HEARTBEAT";
	protected static final String TIMEOUT = "TIMEOUT";
	protected static final String PREPARE = "PREPARE";
	protected static final String HISTORY = "HISTORY";
	protected static final String ACCEPT = "ACCEPT";
	protected static final String NOOP = "NOOP";
	protected static final String ACK = "ACK";
}
