import java.util.ArrayList;


public class History extends ArrayList<Proposal> 
{

	public static History parse(String history)
	{
		try
		{
			String[] proposals = history.split("\\|");
			History result = new History();
			for(String s : proposals)
			{
				Proposal p = Proposal.parse(s);
				result.add(p);
			}
			return result;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public void add(int slot, Proposal p)
	{
		super.add(slot, p);
	}
	
	@Override
	public String toString()
	{
		String result = "";
		for(Proposal p : this)
		{
			result += p.toString() + "|";
		}
		return result.equals("") ? "NOOP" : result;	
	}
}
