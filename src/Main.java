import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;


// -vs, -vc

public class Main {	


		public static InetAddress MCAST_CLIENT;
		public static InetAddress MCAST_SERVER;
		public static final int CLIENT_PORT = 6789;
		public static final int SERVER_PORT = 6790;
		public static final int READ_BUFFER_SIZE = 1024;
		public static int NUM_SERVERS;

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		MCAST_CLIENT = InetAddress.getByName("224.0.0.1");
		MCAST_SERVER = InetAddress.getByName("224.0.0.2");																									
				
				switch(args[0].toLowerCase()) 
				{		
					case "server":
					{
						if (args.length >= 3)
						{
							NUM_SERVERS = Integer.parseInt(args[2]);
						}
						System.out.println("server");
						startServer(Integer.parseInt(args[1]));
						break;
					}
					case "client":
					{	
						new Client();
						break;
					}
					default:
					{		
						System.out.println("MAIN DEFAULT");
					}
				}				
	}

	public static void startServer(int serverID) throws IOException, InterruptedException
	{
		new Server(serverID);
	}
//
//	private static void spawnServers() throws IOException
//	{		
//		ArrayList<Process> processes = new ArrayList<Process>();
//		// make servers
//		for (int serverID = 0; serverID < NUM_SERVERS; ++serverID)
//		{
//			System.out.println("Creating ServerID: " + serverID);			
//			Process p = Runtime.getRuntime().exec("java Main server " + serverID);
//			processes.add(p);
//		}
//		
//		// block for user input on what to do next
//		input:
//			while (true)
//			{
//				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//				String nextInstruction = reader.readLine();
//				String[] tokenized = nextInstruction.split(" ");
//				switch (tokenized[0].toLowerCase())
//				{
//				case "quit":
//					for(Process p : processes)
//					{
//						p.destroy();
//					}				
//					break input;	
//				case "kill":
//					System.out.println("Killing server# " + tokenized[1]);
//					processes.get(Integer.parseInt(tokenized[1])).destroy();
//					break;
//				default:
//					System.out.println("DEFAULT");
//					break;
//				}			
//			}
//	}
//	
//	public static void println(Object o)
//	{
//		if (print)
//			System.out.println(o.toString());
//	}	
}
