import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;


public class CleanUp extends Thread
{
	Server parent;
	Queue<String> acceptMsgs;
	protected boolean stop;
	public CleanUp(Server parent)
	{
		super();
		this.parent = parent;
		stop = false;
		acceptMsgs = new LinkedList<String>();
	}	

	private void generateAcceptMsgs()
	{
		Hashtable<Proposal, Integer> proposal2count = new Hashtable<Proposal, Integer>();
		for(int slotNumber = 0; slotNumber < parent.state.collectedHistories.get(0).size(); slotNumber++)
		{
			for(int historyNum = 0; historyNum < parent.state.collectedHistories.size(); historyNum++){
				for (int i = 0; i < parent.state.collectedHistories.size(); ++i)
				{
					System.out.println("cleanUP, history: " + parent.state.collectedHistories.get(i));
				}
				Proposal p = parent.state.collectedHistories.get(historyNum).get(slotNumber);			
				Integer count = (Integer)proposal2count.get(p) == null ? 1 : (proposal2count.get(p) + 1);
				proposal2count.put(p, count);
			}
						
			for(Proposal p : proposal2count.keySet())
			{
				String acceptMsg = String.format("%s %d %d %s %s", Cmd.ACCEPT, parent.state.prepareNum(), slotNumber, "false", "NOOP");
				if(proposal2count.get(p) >= ServerState.QUORUM)
				{
					acceptMsg = String.format("%s %d %d %s %s", Cmd.ACCEPT, parent.state.prepareNum(), slotNumber, "false", p.value());
				}
				acceptMsgs.offer(acceptMsg);
			}
		}
	}

	@Override 
	public void run()
	{
		generateAcceptMsgs();
		try 
		{
			while(!stop)
			{
				if(!parent.state.cleaningUp)
				{
					parent.state.setState(States.ACCEPT_STATE);
					String msg = acceptMsgs.poll();
					if(acceptMsgs.size() == 0){
						stop = true; 
					}
					System.out.println("THE MSG IS " + msg);
//					parent.serverRouter.broadcast(acceptMsgs.poll());
					parent.serverRouter.broadcast(msg);
				}
			}
		} 
		catch (IOException e) 
		{		
			e.printStackTrace();
		}
		return;
	}
}
