import java.io.IOException;

public class HeartBeat extends Listener
{				
	
	public static final int DEFAULT_HEARTBEAT_INTERVAL = 1000; 	
	public HeartBeat(Channel channel) throws IOException
	{
		super(channel);				
	}
	
	@Override
	public void run()
	{				
		while(true)
		{
			try 
			{
				sleep(DEFAULT_HEARTBEAT_INTERVAL);
				broadcast(Cmd.HEARTBEAT_MSG);
			} 
			catch (InterruptedException | IOException e) 
			{				
				e.printStackTrace();
			}
		}
	}

	@Override
	public void switchBoard(String msg) throws IOException {/* doesn't need to do anything */}
}