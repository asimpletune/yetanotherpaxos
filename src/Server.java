import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Server 
{
	protected Queue<String> msgBuffer; // implemented by ConcurrentLinkedQueue
	protected Listener clientListener;
	protected Listener serverRouter;
	
	protected ServerState state;
	
	public Server(int id) throws IOException, InterruptedException
	{
		state = new ServerState(id);
		
		msgBuffer = new ConcurrentLinkedQueue<String>();
		clientListener = new ClientRouter(this, Channel.defaultChannel);
		serverRouter = new ServerRouter(this, Channel.defaultChannel);			
		
		clientListener.start();
		serverRouter.start();
	}
	
	private void startHeartbeat() throws IOException
	{
		Listener heartListener = new HeartBeat(Channel.defaultChannel);
		heartListener.start();
	}
	
	public void prepare(String msg) throws IOException
	{
		System.out.println("PREPARE: " + msg);
		String[] tokenized = msg.split(" ");
		int newPrepareNum = Integer.parseInt(tokenized[1]);
		if (newPrepareNum > state.prepareNum())
		{
			state.prepareNum(newPrepareNum);
		}
		if (state.isLeader())
		{
			System.out.println("Starting heartbeat");
			startHeartbeat();
//			state.setState(States.ACCEPTING_CLIENT);
		}
		else
		{
			state.setState(States.INITIAL);
		}
		serverRouter.broadcast(String.format("%s %d %s", Cmd.HISTORY, state.serverID(), state.history()));
	}
	
	public CleanUp cleanUp()
	{
		CleanUp clean = new CleanUp(this);
		clean.start();
		return clean;
	}
}
