import java.io.IOException;

class ServerRouter extends Listener
{
	private Server parent;

	public ServerRouter(Server parent, Channel channel) throws IOException 
	{
		super(channel);
		this.parent = parent;		
	}

	@Override
	public void run()
	{
		try
		{																					
			while (true) 
			{								
				String msg = getIncomingMsg();		
				switch(parent.state.getState())
				{
					case INITIAL:
					{
						switchBoard(msg);
						break;
					}
					case ACCEPTING_CLIENT:
					{
						switchBoard(msg);
						break;
					}
					case COLLECTING_HISTORY:
					{
						System.out.println("HISTORY: " + msg);
						collectHistoriesSwitchBoard(msg);
						break;
					}
					case CLEANING_UP:
					{
						cleaningUpSwitchBoard(msg);
					}
					case ACCEPT_STATE:
					{
						acceptSwitchBoard(msg);
						break;
					}
					case COLLECTING_ACKS:
					{
						collectingAcksSwitchBoard(msg);
						break;
					}
					default:
					{
						break;
					}
				}					
			} 
		} 
		catch(IOException e)
		{
			System.out.println("Error in multicast listen: " + e);
		}
		finally
		{
			socket.close();
		}
	}

	@Override
	public void switchBoard(String msg) throws IOException 
	{
		String[] tokenized = msg.split(" ");
		if (tokenized.length > 0)
		{
			String command = tokenized[0];
			switch(command)
			{
				case Cmd.PREPARE:
				{
					parent.prepare(msg);					
					break;
				}
				case Cmd.HISTORY:
				{					
					if (parent.state.isLeader())
					{						
						System.out.println("HISTORY: " + msg);
						parent.state.setState(States.COLLECTING_HISTORY);
						parent.state.collectedHistories.put( Integer.parseInt(tokenized[1]), History.parse(tokenized[2]) );
						if ( parent.state.quorum() )
						{							
							parent.state.cleanUpThread = parent.cleanUp();										
							parent.state.setState(States.CLEANING_UP);
						}
					}
	
					break;
				}
				case Cmd.ACCEPT:
				{					
					parent.state.ackCount = 0;
					parent.state.setState(States.COLLECTING_ACKS);
					boolean shareWithClient = Boolean.parseBoolean(tokenized[3]);
					broadcast( String.format("%s %d %s %s", Cmd.ACK, parent.state.serverID(), shareWithClient, msg) );
				}
				default:
				{
					break;					
				}
			}
		}
	}	

	 // almost no point in putting this here because only the leader can be in this state
	private void collectHistoriesSwitchBoard(String msg) throws IllegalStateException, IOException 
	{		
		String[] tokenized = msg.split(" ");
		if (tokenized.length > 0)
		{
			String command = tokenized[0];				
			switch(command)
			{
				case Cmd.PREPARE:
				{					
					parent.prepare(msg);				
					break;
				}
				case Cmd.HISTORY:
				{
					if (parent.state.isLeader())
					{					
						System.out.println("tok history 1: " + tokenized[1]);
						System.out.println("tok history 2: " + tokenized[2]);
						parent.state.collectedHistories.
						put( Integer.parseInt(tokenized[1]), History.parse(tokenized[2]) );
						if ( parent.state.quorum() )
						{							
							parent.state.cleanUpThread = parent.cleanUp();										
							parent.state.setState(States.CLEANING_UP);
						}
					}
					break;
				}
				default:
				{
					break;
				}
			}
		}

	}


	// Accept state is set in the cleanup thread	
	// almost no point in putting this here because only the leader can be in this state
	private void cleaningUpSwitchBoard(String msg) throws IOException 
	{
		String[] tokenized = msg.split(" ");
		if (tokenized.length > 0)
		{
			String command = tokenized[0];					
			switch(command)
			{
			case Cmd.PREPARE:
			{
				System.out.println("RECEIVED PREPARE IN CLEANUP SWITCH");
				parent.state.cleanUpThread.stop = true;
				parent.prepare(msg);				
				break;
			}				
			default:
				break;
			}
		}		
	}

	/*
	 * This is not done. We need to broadcast, then set out state to accept acks, wait for
	 * a quorum of acks, then broadcast the message. 
	 */
	private void acceptSwitchBoard(String msg) throws IOException 
	{
		String[] tokenized = msg.split(" ");
		if (tokenized.length > 0)
		{
			String command = tokenized[0];					
			switch(command)
			{
				case Cmd.PREPARE:
				{					
					parent.prepare(msg);				
					break;
				}	
				case Cmd.ACCEPT:
				{					
					parent.state.ackCount = 0;
					parent.state.setState(States.COLLECTING_ACKS);
					boolean shareWithClient = Boolean.parseBoolean(tokenized[3]);
					broadcast( String.format("%s %d %s %s", Cmd.ACK, parent.state.serverID(), shareWithClient ,msg) );
				}
				default:
				{
					break;
				}
			}
		}		

	}

	public void collectingAcksSwitchBoard(String msg) throws IOException
	{
		String[] tokenized = msg.split(" ");
		if (tokenized.length > 0)
		{
			String command = tokenized[0];					
			switch(command)
			{
				case Cmd.PREPARE:
				{					
					parent.prepare(msg);				
					break;
				}	
				case Cmd.ACK:
				{					
					parent.state.ackCount++;
					if (parent.state.ackCount >= parent.state.QUORUM)
						
					{
						boolean share = Boolean.parseBoolean(tokenized[2]);
						int startIndex = tokenized[0].length() + tokenized[1].length() 
								+ tokenized[2].length() + tokenized[3].length() 
								+ tokenized[4].length() + tokenized[5].length() + tokenized[6].length() + 7;
						String clientInMsg = msg.substring(startIndex);
						int slotNumber = Integer.parseInt(tokenized[5]);
						System.out.println("MSG: " + msg);
						System.out.println("Client in msg " + clientInMsg);
						Proposal p = new Proposal(parent.state.prepareNum(), clientInMsg);
						parent.state.history().add(slotNumber, p);
						//TODO Write history to disk
						if(share)
						{
							broadcast(String.format("%s %s", Cmd.CLIENT_IN ,clientInMsg));
						}						
						parent.state.setState(States.ACCEPTING_CLIENT);
					}
					break;
				}	
				default:
				{					
					break;
				}

			}
		}
	}
}